package com.example.myapplication;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private final static String EXTRA_MESSAGE = "com.example.myapplication.MESSAGE";
    private final static String DEBUG_TAG = "HttpExample";
    private final int REQ_CODE_SPEECH_INPUT = 100;

    // UI Component
    private TextView txtAlert;
    private TextView txtSpeechInput;
    private TextView printArea;
    private ImageButton btnSpeak;
    private Toolbar myToolbar;

    // Service variance
    private boolean sendTwitter = false;
    private JSONObject myWeather;
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private String last_twitter;
    private String last_weather;
    /*
    Command type:   0 - twitter_context
                    1 - switch_state
                    2 - datetime_text
                    3 - weather
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the user interface layout for this Activity
        setContentView(R.layout.activity_main);

        txtSpeechInput = (TextView) findViewById(R.id.txtSpeechInput);
        txtAlert = (TextView) findViewById(R.id.text_alert);
        btnSpeak = (ImageButton) findViewById(R.id.btnSpeak);
        printArea = (TextView) findViewById(R.id.print_area);

        // set toolbar
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        // set default weather
        myWeather = new JSONObject();

        // set default command type
        // set speech input clicker
        View.OnClickListener clicker = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(DEBUG_TAG, "click");
                promptSpeechInput();
            }
        };
        btnSpeak.setOnClickListener(clicker);

        // Create an instance of GoogleAPIClient
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    // Start the GoogleApiClient when the app starts
    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    // Terminate the GoogleApiClient when the app closed
    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle connectionHint) {

        // If location connection is banned, then do nothing
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            return;
        }


        // get the location information
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            String mLatitude = String.valueOf(mLastLocation.getLatitude());
            String mLongitude = String.valueOf(mLastLocation.getLongitude());

            //printArea.setText(mLatitude+mLongitude);
            Log.d("MSG", mLatitude + mLongitude);

            // start weather query
            new getWeatherTask().execute(mLatitude,mLongitude);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("CONN","Suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("CONN", String.valueOf(connectionResult));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.context_menu, menu);
        return true;
    }

    // Called when the user clicks the send button
    public void watchConnect(View view) {
        //Intent - a context & the app component the system should deliver to
        EditText editText = (EditText) findViewById(R.id.watch_url);
        String watch_url = editText.getText().toString();
        txtAlert.setText("Waiting for response...");
        new GetDataFromPost().execute(watch_url);
    }

    // Uses AsyncTask to create a task away from the main UI thread. This task takes a
    // URL string and uses it to create an HttpUrlConnection. Once the connection
    // has been established, the AsyncTask downloads the contents of the webpage as
    // an InputStream. Finally, the InputStream is converted into a string, which is
    // displayed in the UI by the AsyncTask's onPostExecute method.
    private class GetDataFromPost extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... urls) {
                //parameters comes from the execute() call: params[0] is the url.
                try {
                    String return_text = SendPostRequest(urls[0]);
                    if (return_text != "") {
                        return return_text;
                    } else {
                        return "Nothing received";
                    }

                } catch (IOException e) {
                    return "Unable to retrieve web page. Url may be invalid: " + e.getMessage();
                }
            }
        @Override
        protected void onPostExecute(String result) {
            txtAlert.setText(result);
        }
    }

    public String getSystemTime() {
        Calendar calendar = Calendar.getInstance();
        String datetime_text = calendar.getTime().toString();
        String[] date_array = datetime_text.split(" ");
        JSONObject jsDate = new JSONObject();
        // Month
        String month;
        switch (date_array[1]) {
            case "Jan":
                month = "1";
                break;
            case "Feb":
                month = "2";
                break;
            case "Mar":
                month = "3";
                break;
            case "Apr":
                month = "4";
                break;
            case "May":
                month = "5";
                break;
            case "Jun":
                month = "6";
                break;
            case "Jul":
                month = "7";
                break;
            case "Aug":
                month = "8";
                break;
            case "Sep":
                month = "9";
                break;
            case "Oct":
                month = "10";
                break;
            case "Nov":
                month = "11";
                break;
            default:
                month = "12";
                break;
        }
        // weekday
        String weekday;
        switch (date_array[0]) {
            case "Mon":
                weekday = "1";
                break;
            case "Tue":
                weekday = "2";
                break;
            case "Wed":
                weekday = "3";
                break;
            case "Thu":
                weekday = "4";
                break;
            case "Fri":
                weekday = "5";
                break;
            case "Sat":
                weekday = "6";
                break;
            default:
                weekday = "7";
                break;
        }
        //time
        String[] time = date_array[3].split(":");
        String time_all = date_array[5]+','+ month +','+ date_array[2] +','+
                weekday +','+ time[0] +','+ time[1] +','+ time[2];

//            jsDate.put("year", date_array[5]);
//            jsDate.put("month", month);
//            jsDate.put("date", date_array[2]);
//            jsDate.put("weekday", weekday);
//            jsDate.put("hour", time[0]);
//            jsDate.put("minute", time[1]);
//            jsDate.put("second", time[2]);

        return time_all;
    }

    // the AsynTask for the Application to get weather information
    private class getWeatherTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String lat = params[0];
            String lon = params[1];
            getWeather(lat,lon);
            return myWeather.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            //printArea.setText(result);
        }
    }

    public void getWeather(String lat, String lon) {
//        JSONObject jObj = new JSONObject(data);
        String url_weather = "http://api.openweathermap.org/data/2.5/weather?appid=42cefeb332fe82a9f53d13d7b1bdbec7";
        String location = String.format("&lat=%s&lon=%s", lat, lon);
        url_weather += location;

        Log.d(DEBUG_TAG, url_weather);

        String response = "";
        try {
            // Connected to server and retrieve weather
            URL url = new URL(url_weather);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000 /*milliseconds*/);
            conn.setConnectTimeout(15000/*milliseconds*/);
            conn.setDoOutput(true);
            conn.connect();

            int responseCode = conn.getResponseCode();
            Log.d(DEBUG_TAG, "The reseponse is:"+responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                // Get the feedback weather information
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));
                String line = br.readLine();
                while (line!=null){
                    response += line;
                    line = br.readLine();
                }

                // Parse weather information
                JSONObject jsObj = new JSONObject(response);
                JSONArray jsArray = jsObj.getJSONArray("weather");
                JSONObject jsWeather = jsArray.getJSONObject(0);
                JSONObject jsTemperature = jsObj.getJSONObject("main");
                JSONObject jsWind = jsObj.getJSONObject("wind");

                myWeather.put("weather", jsWeather.getString("description"));
                myWeather.put("temperature", String.format("%.1f C",
                        (jsTemperature.getDouble("temp")-273)));
                myWeather.put("wind",  String.format("%.1f",jsWind.getDouble("speed")));
                last_weather = jsWeather.getString("description")+",temp:"+String.format("%.1f C",
                        (jsTemperature.getDouble("temp")-273));
                printArea.setText(last_weather);
            }
        } catch (Exception e){
            e.printStackTrace();
            //printArea.setText(e.getMessage());
        }
        Log.d(DEBUG_TAG,response);
    }

    // send the post request to the target URL
    @Nullable
    private String SendPostRequest(String myurl) throws IOException {

        // Only display the first 500 characters of the retrieved
        // web page content.
        String response = "";
        OutputStream os;
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // set up connection
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setReadTimeout(10000 /*milliseconds*/);
            conn.setConnectTimeout(15000/*milliseconds*/);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            // Set up json elements
            // Initialize the transmission data
            String message;

            // Recognize the voice command
            // and put the correspond data in json string
            String translation_text = txtSpeechInput.getText().toString();
            Log.d(DEBUG_TAG,translation_text);
            switch (translation_text) {
                case "XYZ":
                    if (!sendTwitter) {
                        Random rnd = new Random();
                        int x = rnd.nextInt(10);
                        int y = rnd.nextInt(10);
                        int z = rnd.nextInt(10);
                        message = "{ \"X\":" + x + ", \"Y\":" + y + ", \"Z\":"+ z +" }";
                        break;
                    }
                case "send Twitter":
                    if (sendTwitter) {
                        last_twitter = translation_text;
                        message = "{0,Twitter:," + last_twitter +'}';
                        new PostTwitterTask().execute();
                        sendTwitter = false;
                        break;
                    } else {
                        promptSpeechInput();
                        sendTwitter = true;
                        return null;
                    }
                case "check Twitter":
                    if (!sendTwitter) {
                        message = "{0,Twitter:," + last_twitter + '}';
                        break;
                    }
                case "display time":
                    if (!sendTwitter) {
                        String datetime = getSystemTime();
                        message = "{1," + datetime + '}';
                        break;
                    }
                case "check weather":
                case "display weather":
                    if (!sendTwitter) {
                        message = "{0,"+last_weather + '}';
                        break;
                    }
                default:
                    if (sendTwitter){
                        last_twitter = translation_text;
                        message = "{0,Twitter:," + last_twitter + '}';
                        new PostTwitterTask().execute();
                        sendTwitter = false;
                        break;
                    }
                    return null;
            }

            Log.d(DEBUG_TAG, message);

            // transmit the data to server
            os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(message);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();
            Log.d(DEBUG_TAG, new String("" + responseCode));
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));
                String line = br.readLine();
                while (line != null) {
                    response += line;
                    line = br.readLine();
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    private class PostTwitterTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            //parameters comes from the execute() call: params[0] is the url.
                String return_text = PostTwitter();
                if (return_text != "") {
                    return "Success!";
                } else {
                    return "Nothing received";
                }
        }

        @Override
        protected void onPostExecute(String result) {
//            txtAlert.setText(result);
        }
    }

    public String PostTwitter(){
        // send your last twitter
        String url_twitter = "https://maker.ifttt.com/trigger/status_update/with/key/naC70KgPn0Qt0F4gHW3dWruXbMDPqh0SepzpCIZ3LYo";
        String response= " ";
        OutputStream os;
        try{
            URL url = new URL(url_twitter);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // set up connection
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setReadTimeout(10000 /*milliseconds*/);
            conn.setConnectTimeout(15000/*milliseconds*/);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            JSONObject json = new JSONObject();
            json.put("value1", last_twitter);

            os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(json.toString());
            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();
            Log.d(DEBUG_TAG, new String("" + responseCode));
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));
                String line = br.readLine();
                while (line != null) {
                    response += line;
                    line = br.readLine();
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    // Recording
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException ea) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    // Receiving speech input
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(
                            RecognizerIntent.EXTRA_RESULTS);
                    txtSpeechInput.setText(result.get(0));
                    if (txtSpeechInput.getText().toString() != "send Twitter") {
                        // send command
                        watchConnect(btnSpeak);
                    }
                }
                break;
            }
        }
    }
}
