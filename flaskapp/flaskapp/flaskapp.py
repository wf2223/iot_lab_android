from flask import Flask
from flask_pymongo import PyMongo
from pymongo import MongoClient
from flask import jsonify
from flask import request
from flask import render_template
import json
import time

app = Flask(__name__)
@app.route('/')
def hello():
    return 'hello'

client = MongoClient()
db = client.restdb
result = db.acc.remove({})

app.config['MONGO_DBNAME'] = 'restdb'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/restdb'

@app.route('/POST', methods=['POST'])
def get_acc():
    x = request.json['X']
    y = request.json['Y']
    z = request.json['Z']
    ISOTIMEFORMAT = '%Y-%m-%d %X'
    timetime = time.strftime(ISOTIMEFORMAT, time.localtime())	
    db_id = db.acc.insert({'X': x, 'Y': y, 'Z': z, 't': timetime})
    new_acc = db.acc.find_one({'_id': db_id})
    output = {'X': new_acc['X'], 'Y': new_acc['Y'], 'Z': new_acc['Z'], 't': new_acc['t']}
    return jsonify({'result':output})


@app.route('/chart/', methods=['GET'])
def draw_chart():
    x_all = []
    y_all = []
    z_all = []
    for s in db.acc.find():
	x_all.append([str(s['t']),s['X']])
	y_all.append([str(s['t']),s['Y']])
	z_all.append([str(s['t']),s['Z']])
    return render_template('live_update_script_main.html',x_all=x_all,y_all=y_all,z_all=z_all)
	

@app.route('/GET/', methods=['GET'])
def get_all_acc():
    output = []
    for s in db.acc.find():
        output.append({'X': s['X'], 'Y': s['Y'], 'Z': s['Z'], 't': s['t']})
    return jsonify({'result':output})


@app.route('/GET2/', methods=['GET'])
def get_last_acc():
    output = []
    for s in db.acc.find().sort('_id',-1).limit(1):
        output.append({'X': s['X'], 'Y': s['Y'], 'Z': s['Z'], 't': s['t']})
    return jsonify({'result':output})


@app.route('/show/')
@app.route('/show/<flag>')
def show(flag=None):
    return render_template('live_update_script.html',flag=flag)

@app.route('/show_php/')
def show_php():
    return render_template('echo.php')


if __name__ == '__main__':
    app.run(debug=True)
